@include("frontend.body.header")

<body class="p-0 m-0 container-fluid">

    @include("frontend.body.navbar")

    @include("frontend.body.banner")

    @include("frontend.body.menu")

    <div class="container-fluid">
        <div class="p-12 row">
            <div class="mb-3">
                <h4>1. Introduction:</h4>
                <p>
                    This Privacy Policy governs the manner in which [News Media Website] collects, uses, maintains, and discloses information from users (referred to as "you" or "users") of our website. We are committed to protecting your privacy and ensuring the confidentiality of any personal information you provide to us. By accessing and using our website, you consent to the practices outlined in this Privacy Policy.
                </p>
            </div>
            <div class="mb-3">
                <h4>2. Information we collect:</h4>
                <p>
                    We may collect personal information from users in various ways, including but not limited to when you visit our website, register for an account, subscribe to our newsletters, submit comments or feedback, or interact with our content. The types of personal information we may collect include your name, email address, postal address, and any other information you voluntarily provide to us.
                </p>
            </div>
            <div class="mb-3">
                <h4>3. How we use collected information:</h4>
                <p>
                    We may use the information we collect from users for the following purposes:
                </p>
                <p>
                    To improve user experience: We may use feedback you provide to enhance our website's features and content.
                    To send periodic emails: We may use your email address to respond to inquiries, send updates, news, or other information related to our services.
                </p>
                <p>
                    To personalize user experience: We may use information to understand how our users interact with our website and tailor our content to their interests.
                    To administer contests, promotions, or surveys: If you participate in any of these activities, we may use the provided information to manage the event and contact you as necessary.
                </p>
            </div>

            <div class="mb-3">
                <h4>4. How we protect your information:</h4>
                <p>
                    We adopt appropriate data collection, storage, and processing practices and security measures to protect against unauthorized access, alteration, disclosure, or destruction of your personal information, username, password, transaction information, and data stored on our website.
                </p>
            </div>

            <div class="mb-3">
                <h4>5. Sharing your personal information:</h4>
                <p>
                    We do not sell, trade, or rent users' personal information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates, and advertisers for the purposes outlined above.
                </p>
            </div>

            <div class="mb-3">
                <h4>6. Third-party websites:</h4>
                <p>
                    Users may find advertising or other content on our website that links to the websites and services of our partners, advertisers, sponsors, licensors, and other third parties. We do not control the content or links that appear on these websites and are not responsible for their privacy practices. Browsing and interaction on any other website, including those that have a link to our website, is subject to that website's own terms and policies.
                </p>
            </div>
            <div class="mb-3">
                <h4>
                    7. Contact us:
                </h4>
                <p>
                    If you have any questions about this Privacy Policy, the practices of this website, or your dealings with this site, please contact us.
                </p>
            </div>
        </div>
    </div>

    @include("frontend.body.footer")
